// Módulo interno do Node.js
const util = require('util')
const getAddressAsync = util.promisify(getAddress)

function getUser() {
  return new Promise( resolveUser = (resolve, reject) => {
    setTimeout(() => {
      return resolve({
        id: 1,
        name: 'John',
        birthday: new Date()
      })
    }, 1000)
  }) 
}

function getPhone(idUser) {
  return new Promise(resolvePhone = (resolve, reject) => {
    setTimeout(() => {
      return resolve({
        phone: '75998989898'
      })
    }, 2000)
  })
}

function getAddress(idUser, callback) {
  setTimeout(() => {
    return callback(null, {
      address: 'New Street',
      number: 0
    })
  }, 2000)
}

async function main() {
  try {
    const user = await getUser()
    // const phone = await getPhone(user.id)
    // const address = await getAddressAsync(user.id)
    const result = await Promise.all([
      getPhone(user.id),
      getAddressAsync(user.id)
    ])
    const address = result[1]
    const phone = result[0]
    console.log(`
      Nome: ${user.name},
      Telefone: ${phone.phone}
      Address: ${address.address}
    `)
  } catch (error) {
    console.log('Error: ', error)
  }
}

main()