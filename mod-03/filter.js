const { getPeople } = require('./service')

Array.prototype.myFilter = function(callback) {
  const list = []

  for (index in this) {
    const item = this[index]
    const result = callback(item, index, this)
    if (!result) continue

    list.push(item)
  }

  return list
}

async function main() {
  try {
    const { results } = await getPeople('a')

    // const familySkywalker = results.filter((item) => {
    //   const result = item.name.toLowerCase().indexOf('skywalker') !== -1
    //   return result
    // })

    const familySkywalker = results.myFilter((item) => {
      return item.name.toLowerCase().indexOf('skywalker') !== -1
    })

    const names = familySkywalker.map((people) => people.name)

    console.log(names)
  } catch (err) {
    console.log('Error: ', err)
  }
}

main()