const service = require('./service')

Array.prototype.myMap = function(callback) {
  const newArrayMap = []
  for (let index = 0; index <= this.length -1; index++ ) {
    const result = callback(this[index], index)
    newArrayMap.push(result)
  }

  return newArrayMap
}

async function main() {
  try {
    const result = await service.getPeople('a')
    // const names = []
    // result.results.forEach(element => {
    //   names.push(element.name)
    // });
    
    // const names = result.results.map((item) => {
    //   return item.name
    // })

    // const names = result.results.map((item) => item.name)

    const names = result.results.myMap((item, index) => {
      return `[${index}]${item.name}`
    })

    console.log('Nomes: ', names)
  } catch (err) {
    console.log('Erro: ', err)
  }
}

main()