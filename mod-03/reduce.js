const { getPeople } = require('./service')

Array.prototype.myReduce = function (callback, initialValue) {
  let finalValue = typeof initialValue !== undefined ? initialValue : this[0]

  for (let index = 0; index <= this.length - 1; index++) {
    finalValue = callback(finalValue, this[index], this)
  }

  return finalValue
}

async function main() {
  try {
    // const { results } = await getPeople('a')
    // const height = results.map((item) => parseInt(item.height))
    // console.log('Pesos: ', height)

    // const total = height.reduce((prev, next) => {
    //   return prev + next
    // }, 0)

    const myList = [
      ['Diego', 'Capella'],
      ['NodeBr', 'Git Brasil']
    ]

    const total = myList.myReduce((prev, next) => {
      return prev.concat(next)
    }, [])
    .join(', ')

    console.log('Total: ', total)
  } catch (err) {
    console.log(`Error: `, err)
  }
}

main()